FROM registry.gitlab.com/makotaka/images:v1.21.0
ARG VER
RUN apk add --no-cache curl bash jq\
  && curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/download/$VER/argocd-linux-amd64 \
  && chmod +x /usr/local/bin/argocd


ENTRYPOINT ["/usr/local/bin/argocdl"]
CMD [""]

